package de.tarent.plexus.security.webservice;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;
import org.evolvis.GForgeAPIPortTypeProxy;

import de.tarent.plexus.security.config.ConfigReader;

/**
 * Just a wrapper for the GForgeAPIPortTypeProxy to support Basic HTTP Auth.
 * Should be a delegate instead of a subclass or done by an AOP Advice, but I
 * was a little lazy ...
 * 
 * Checks if the <code>HTTP_AUTH_PROP</code> property is set to "true" (in the
 * gforge.cnf) and uses the configured username and password to do the auth.
 * 
 * FIXME: why do i get "org.xml.sax.SAXParseException: Premature end of file" ?
 * when using the lds-evolvis. Http-Header "User-Agent: Axis 1.4" causes this
 * ...
 * 
 * @author Fabian Bieker
 * 
 */
public class HttpAuthGForgeAPIPortTypeProxy extends GForgeAPIPortTypeProxy {

    public static final String HTTP_AUTH_PROP = "httpAuth";

    private static final Logger log = Logger.getLogger(HttpAuthGForgeAPIPortTypeProxy.class);

    public HttpAuthGForgeAPIPortTypeProxy() {
        super();
        configureHttpAuth((Stub) getGForgeAPIPortType());
    }

    public static void configureHttpAuth(Stub stub) {

        final ConfigReader config = ConfigReader.getInstance();

        if (!config.hasConfig(HTTP_AUTH_PROP)) {
            log.debug("not setting HTTP Auth - " + HTTP_AUTH_PROP
                    + " property not set");
            return;
        }

        final String val = config.getConfig(HTTP_AUTH_PROP);
        if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("yes")) {
            final String user = config.getConfig("user");
            final String pass = config.getConfig("pwd");
            log.debug("using '" + user + "' / '***' for HTTP Auth");
            stub._setProperty(Call.USERNAME_PROPERTY, user);
            stub._setProperty(Call.PASSWORD_PROPERTY, pass);
        } else {
            log.debug("not setting HTTP Auth " + HTTP_AUTH_PROP
                    + " property is to " + val);
        }

    }
}
