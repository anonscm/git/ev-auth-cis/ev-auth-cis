/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.codehaus.plexus.redback.rbac.Operation;
import org.codehaus.plexus.redback.rbac.Permission;
import org.codehaus.plexus.redback.rbac.Resource;
import org.evolvis.GForgeAPIPortTypeProxy;

import de.tarent.plexus.security.config.ConfigReader;
import de.tarent.plexus.security.webservice.HttpAuthGForgeAPIPortTypeProxy;

public class GforgePermission implements Permission{

	private static final Logger log = Logger.getLogger(GforgePermission.class);
	
	private String description;
	private String name;
	private Operation operation;
	private Resource resource;
	
	public GforgePermission(){}
	
	public GforgePermission(String name, String description, Operation operation, Resource resource){
		this.name = name;
		this.description = description;
		this.operation = operation;
		this.resource = resource;
	}
	
	public String getDescription() {
		return this.description;
	}

	public String getName() {
		return this.name;
	}

	public Operation getOperation() {
		return this.operation;
	}

	public Resource getResource() {
		return this.resource;
	}

	public boolean isPermanent() {
		return true;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;		
	}

	public void setPermanent(boolean arg0) {}

	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	public static Hashtable<String, Permission> initializeAll() {
		
		GforgeEntityManager manager = GforgeEntityManager.getInstance();
		Hashtable<String, Permission> data = new Hashtable<String, Permission>();
		data.put("Guest Permission", new GforgePermission("Guest Permission", "", manager.getOperation("guest-access"), manager.getResource("*")));	
		data.put("Edit User Data by Username", new GforgePermission("Edit User Data by Username", "", manager.getOperation("Edit User Data by Username"), manager.getResource("${username}")));
		data.put("Edit Redback Configuration", new GforgePermission("Edit Redback Configuration", "", manager.getOperation("configuration-edit"), manager.getResource("*")));
		data.put("User RBAC Management", new GforgePermission("User RBAC Management", "", manager.getOperation("user-management-rbac-admin"), manager.getResource("*")));
		data.put("Drop Roles for Anyone", new GforgePermission("Drop Roles for Anyone", "", manager.getOperation("user-management-role-drop"), manager.getResource("*")));
		data.put("Grant Roles for Anyone", new GforgePermission("Grant Roles for Anyone", "", manager.getOperation("user-management-role-grant"), manager.getResource("*")));
		data.put("Create Users", new GforgePermission("Create Users", "", manager.getOperation("user-management-user-create"), manager.getResource("*")));
		data.put("Delete Users", new GforgePermission("Delete Users", "", manager.getOperation("user-management-user-delete"), manager.getResource("*")));
		data.put("Edit Users", new GforgePermission("Edit Users", "", manager.getOperation("user-management-user-edit"), manager.getResource("*")));
		data.put("Access Users Roles", new GforgePermission("Access Users Roles", "", manager.getOperation("user-management-user-role"), manager.getResource("*")));
		data.put("Access User List", new GforgePermission("Access User List", "", manager.getOperation("user-management-user-list"), manager.getResource("*")));
		data.put("Continuum Guest Permission", new GforgePermission("Continuum Guest Permission", "", manager.getOperation("continuum-guest"), manager.getResource("*")));
		data.put("Manage Continuum Configuration", new GforgePermission("Manage Continuum Configuration", "", manager.getOperation("continuum-manage-configuration"), manager.getResource("*")));
		data.put("Manage Continuum Schedules", new GforgePermission("Manage Continuum Schedules", "", manager.getOperation("continuum-manage-schedules"), manager.getResource("*")));
		data.put("Add Group to Continuum", new GforgePermission("Add Group to Continuum", "", manager.getOperation("continuum-add-group"), manager.getResource("*")));
		data.put("Manage Continuum Users", new GforgePermission("Manage Continuum Users", "", manager.getOperation("continuum-manage-users"), manager.getResource("*")));
		data.put("Continuum Manage Users - *", new GforgePermission("Continuum Manage Users - *", "", manager.getOperation("continuum-manage-users"), manager.getResource("*")));
		data.put("Continuum Manage User Roles - *", new GforgePermission("Continuum Manage User Roles - *", "", manager.getOperation("user-management-user-role"), manager.getResource("*")));
		
		GForgeAPIPortTypeProxy proxy = new HttpAuthGForgeAPIPortTypeProxy();
		proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
		try {
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			String[] projects = proxy.getPublicProjectNames(session);
			for(String project : projects){
				data.put("Continuum Grant Group Roles - " + project, new GforgePermission("Continuum Grant Group Roles - " + project, "", manager.getOperation("user-management-role-grant"), manager.getResource(project)));
				data.put("Continuum Build Group - " + project, new GforgePermission("Continuum Build Group - " + project, "", manager.getOperation("continuum-build-group"), manager.getResource(project)));
				data.put("Continuum Remove Group - " + project, new GforgePermission("Continuum Remove Group - " + project, "", manager.getOperation("continuum-remove-group"), manager.getResource(project)));
				data.put("Continuum Modify Group - " + project, new GforgePermission("Continuum Modify Group - " + project, "", manager.getOperation("continuum-modify-group"), manager.getResource(project)));
				data.put("Continuum Add Group Build Definition - " + project, new GforgePermission("Continuum Add Group Build Definition - " + project, "", manager.getOperation("continuum-add-group-build-definition"), manager.getResource(project)));
				data.put("Continuum Remove Group Build Definition - " + project, new GforgePermission("Continuum Remove Group Build Definition - " + project, "", manager.getOperation("continuum-remove-group-build-definition"), manager.getResource(project)));
				data.put("Continuum Modify Group Build Definition - " + project, new GforgePermission("Continuum Modify Group Build Definition - " + project, "", manager.getOperation("continuum-modify-group-build-definition"), manager.getResource(project)));
				data.put("Continuum Add Group Notifier - " + project, new GforgePermission("Continuum Add Group Notifier - " + project, "", manager.getOperation("continuum-add-group-notifier"), manager.getResource(project)));
				data.put("Continuum Remove Group Notifier - " + project, new GforgePermission("Continuum Remove Group Notifier - " + project, "", manager.getOperation("continuum-remove-group-notifier"), manager.getResource(project)));
				data.put("Continuum Modify Group Notifier - " + project, new GforgePermission("Continuum Modify Group Notifier - " + project, "", manager.getOperation("continuum-modify-group-notifier"), manager.getResource(project)));
				data.put("Continuum Add Project Build Definition - " + project, new GforgePermission("Continuum Add Project Build Definition - " + project, "", manager.getOperation("continuum-add-project-build-definition"), manager.getResource(project)));
				data.put("Continuum Remove Project Build Definition - " + project, new GforgePermission("Continuum Remove Project Build Definition - " + project, "", manager.getOperation("continuum-remove-project-build-definition"), manager.getResource(project)));
				data.put("Continuum Modify Project Build Definition - " + project, new GforgePermission("Continuum Modify Project Build Definition - " + project, "", manager.getOperation("continuum-modify-project-build-definition"), manager.getResource(project)));
				data.put("Continuum Add Project Notifer - " + project, new GforgePermission("Continuum Add Project Notifer - " + project, "", manager.getOperation("continuum-add-project-notifier"), manager.getResource(project)));
				data.put("Continuum Remove Project Notifer - " + project, new GforgePermission("Continuum Remove Project Notifer - " + project, "", manager.getOperation("continuum-remove-project-notifier"), manager.getResource(project)));
				data.put("Continuum Modify Project Notifer - " + project, new GforgePermission("Continuum Modify Project Notifer - " + project, "", manager.getOperation("continuum-modify-project-notifier"), manager.getResource(project)));
				data.put("Continuum Build Project in Group - " + project, new GforgePermission("Continuum Build Project in Group - " + project, "", manager.getOperation("continuum-build-project-in-group"), manager.getResource(project)));
				data.put("Continuum Add Project To Group - " + project, new GforgePermission("Continuum Add Project To Group - " + project, "", manager.getOperation("continuum-add-project-to-group"), manager.getResource(project)));
				data.put("Continuum Remove Project from Group - " + project, new GforgePermission("Continuum Remove Project from Group - " + project, "", manager.getOperation("continuum-remove-project-from-group"), manager.getResource(project)));
				data.put("Continuum Modify Project in Group - " + project, new GforgePermission("Continuum Modify Project in Group - " + project, "", manager.getOperation("continuum-modify-project-in-group"), manager.getResource(project)));
				data.put("Continuum View Group - " + project, new GforgePermission("Continuum View Group - " + project, "", manager.getOperation("continuum-view-group"), manager.getResource(project)));
			}
			proxy.logout(session);
		} catch (RemoteException e) {
			log.error("failed to init GforgePermission (some issue with the soap service?)", e);
		}
		return data;
	}

}