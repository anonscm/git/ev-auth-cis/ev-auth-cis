/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.codehaus.plexus.redback.rbac.Resource;
import org.evolvis.GForgeAPIPortTypeProxy;

import de.tarent.plexus.security.config.ConfigReader;
import de.tarent.plexus.security.webservice.HttpAuthGForgeAPIPortTypeProxy;

public class GforgeResource implements Resource{

	private static final Logger log = Logger.getLogger(GforgeResource.class);
	
	private String identifier;
	
	public GforgeResource(){}
	
	public GforgeResource(String identifier){
		this.identifier = identifier;
	}
	
	public String getIdentifier() {
		return this.identifier;
	}

	public boolean isPattern() {
		return false;
	}

	public boolean isPermanent() {
		return true;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public void setPattern(boolean pattern) {}

	public void setPermanent(boolean permanent) {}

	public static Hashtable<String, Resource> initializeAll(){
		Hashtable<String, Resource> data = new Hashtable<String, Resource>();
		data.put("*", new GforgeResource("*"));
		data.put("${username}", new GforgeResource("${username}"));
		GForgeAPIPortTypeProxy proxy = new HttpAuthGForgeAPIPortTypeProxy();
		proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
		try {
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			String[] projects = proxy.getPublicProjectNames(session);
			for(String project : projects){
				data.put(project, new GforgeResource(project));
			}
			proxy.logout(session);
		} catch (RemoteException e) {
			log.error("failed to init GforgeResource (some issue with the soap service?)", e);
		}
		return data;
	}
}
