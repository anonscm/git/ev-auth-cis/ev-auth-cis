/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import org.codehaus.plexus.redback.rbac.Operation;

import java.util.Hashtable;

public class GforgeOperation implements Operation{

	private String description;
	private String name;
	
	public GforgeOperation(){};
	
	public GforgeOperation(String name, String description){
		this.name = name;
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}

	public String getName() {
		return this.name;
	}

	public boolean isPermanent() {
		return true;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPermanent(boolean arg0) {}
	
	public static Hashtable<String, Operation> initializeAll(){
		Hashtable<String, Operation> data = new Hashtable<String, Operation>();
		data.put("configuration-edit", new GforgeOperation("configuration-edit","edit configuration"));
		data.put("continuum-add-group", new GforgeOperation("continuum-add-group","Continuum Add Group"));
		data.put("continuum-add-group-build-definition", new GforgeOperation("continuum-add-group-build-definition","Continuum Add Group Build Definition"));
		data.put("continuum-add-group-notifier", new GforgeOperation("continuum-add-group-notifier","Continuum Add Group Notifier"));
		data.put("continuum-add-project-build-definition", new GforgeOperation("continuum-add-project-build-definition","Continuum Add Project Build Definition"));
		data.put("continuum-add-project-notifier", new GforgeOperation("continuum-add-project-notifier","Continuum Add Project Notifier"));
		data.put("continuum-add-project-to-group", new GforgeOperation("continuum-add-project-to-group","Continuum Add Project to Group"));
		data.put("continuum-build-group", new GforgeOperation("continuum-build-group","Continuum Build Group"));
		data.put("continuum-build-project-in-group", new GforgeOperation("continuum-build-project-in-group","Continuum Build Project in Group"));
		data.put("continuum-guest", new GforgeOperation("continuum-guest","Continuum Guest Operations"));
		data.put("continuum-manage-configuration", new GforgeOperation("continuum-manage-configuration","Manage Continuum Configuration"));
		data.put("continuum-manage-schedules", new GforgeOperation("continuum-manage-schedules","Manage Continuum Scheduling"));
		data.put("continuum-manage-users", new GforgeOperation("continuum-manage-users","Manage Continuum Users"));
		data.put("continuum-modify-group", new GforgeOperation("continuum-modify-group","Continuum Modify Group"));
		data.put("continuum-modify-group-build-definition", new GforgeOperation("continuum-modify-group-build-definition","Continuum Modify Group Build Definition"));
		data.put("continuum-modify-group-notifier", new GforgeOperation("continuum-modify-group-notifier","Continuum Modify Group Notifier"));
		data.put("continuum-modify-project-build-definition", new GforgeOperation("continuum-modify-project-build-definition","Continuum Modify Project Build Definition"));
		data.put("continuum-modify-project-in-group", new GforgeOperation("continuum-modify-project-in-group","Continuum Modify Project in Group"));
		data.put("continuum-modify-project-notifier", new GforgeOperation("continuum-modify-project-notifier","Continuum Modify Project Notifier"));
		data.put("continuum-remove-group", new GforgeOperation("continuum-remove-group","Continuum Remove Group"));
		data.put("continuum-remove-group-build-definition", new GforgeOperation("continuum-remove-group-build-definition","Continuum Remove Group Build Definition"));
		data.put("continuum-remove-group-notifier", new GforgeOperation("continuum-remove-group-notifier","Continuum Remove Group Notifier"));
		data.put("continuum-remove-project-build-definition", new GforgeOperation("continuum-remove-project-build-definition","Continuum Remove Project Build Definition"));
		data.put("continuum-remove-project-from-group", new GforgeOperation("continuum-remove-project-from-group","Continuum Remove Project from Group"));
		data.put("continuum-remove-project-notifier", new GforgeOperation("continuum-remove-project-notifier","Continuum Remove Project Notifier"));
		data.put("continuum-view-group", new GforgeOperation("continuum-view-group","Continuum View Groups"));
		data.put("guest-access", new GforgeOperation("guest-access","access guest"));
		data.put("user-management-rbac-admin", new GforgeOperation("user-management-rbac-admin","administer rbac"));
		data.put("user-management-role-drop", new GforgeOperation("user-management-role-drop","drop role"));
		data.put("user-management-role-grant", new GforgeOperation("user-management-role-grant","grant role"));
		data.put("user-management-user-create", new GforgeOperation("user-management-user-create","create user"));
		data.put("user-management-user-delete", new GforgeOperation("user-management-user-delete","delete user"));
		data.put("user-management-user-edit", new GforgeOperation("user-management-user-edit","edit user"));
		data.put("user-management-user-list", new GforgeOperation("user-management-user-list","list users"));
		data.put("user-management-user-role", new GforgeOperation("user-management-user-role","user roles"));
		return data;
	}
	
}
