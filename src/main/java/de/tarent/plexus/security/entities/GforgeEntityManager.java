/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicReference;

import org.codehaus.plexus.redback.rbac.Operation;
import org.codehaus.plexus.redback.rbac.Permission;
import org.codehaus.plexus.redback.rbac.Resource;
import org.codehaus.plexus.redback.rbac.Role;
import org.codehaus.plexus.redback.rbac.UserAssignment;

public class GforgeEntityManager {
	
	private static final AtomicReference<GforgeEntityManager> instance = 
	    new AtomicReference<GforgeEntityManager>();
	private Hashtable<String, Role> roles;
	private Hashtable<String, Permission> permissions;
	private Hashtable<String, Resource> resources;
	private Hashtable<String, UserAssignment> userassignments;
	private Hashtable<String, Operation> operations;
	private long lastTimeUpdateRoles;
	private long lastTimeUpdatePermissions;
	private long lastTimeUpdateResources;
	private long lastTimeUpdateUserAssignments;
	private long lastTimeUpdateOperations;
	
	private GforgeEntityManager(){}
	
	public Hashtable<String, Permission> getAllPermissions(){
		if(checkIfEntytyShouldBeUpdatet("permission") || permissions == null){
			permissions = GforgePermission.initializeAll();
			lastTimeUpdatePermissions = new Date().getTime();
		}
		return this.permissions;
	}
	
	public Hashtable<String, Operation> getAllOperations(){
		if(checkIfEntytyShouldBeUpdatet("operation") || operations == null){
			this.operations = GforgeOperation.initializeAll();
			lastTimeUpdateOperations = new Date().getTime();
		}
		return this.operations;
	}
	
	public Hashtable<String, UserAssignment> getAlluserAssignments(){
		if(checkIfEntytyShouldBeUpdatet("userassignment") || userassignments == null){
			this.userassignments = GforgeUserAssignment.initializeAll();
			lastTimeUpdateUserAssignments = new Date().getTime();
		}
		return this.userassignments;
	}
	
	public Hashtable<String, Resource> getAllResources(){
		if(checkIfEntytyShouldBeUpdatet("resource") || resources == null){
			this.resources = GforgeResource.initializeAll();
			lastTimeUpdateResources = new Date().getTime();
		}	
		return this.resources;
	}
	
	public Hashtable<String, Role> getAllRoles(){
		if(checkIfEntytyShouldBeUpdatet("role") || roles == null){
			this.roles = GforgeRole.initializeAll();
			lastTimeUpdateRoles = new Date().getTime();
		}
		return this.roles;
	}
	
	public Resource getResource(String name){
		return getAllResources().get(name);
	}
	
	public Permission getPermission(String name){
		return getAllPermissions().get(name);
	}
	
	public Role getRoleByName(String name){
		return getAllRoles().get(name);
	}
	
	public Operation getOperation(String name){
		return getAllOperations().get(name);
	}
	
	public UserAssignment getUserassignment(String name){
		return getAlluserAssignments().get(name);
	}
	
	public boolean checkIfEntytyShouldBeUpdatet(String entity){
		long lastTimeChecked = 0;
		if(entity.equalsIgnoreCase("role")){
			lastTimeChecked = lastTimeUpdateRoles;
		} else if (entity.equalsIgnoreCase("permission")){
			lastTimeChecked = lastTimeUpdatePermissions;
		} else if (entity.equalsIgnoreCase("resource")){
			lastTimeChecked = lastTimeUpdateResources;
		} else if (entity.equalsIgnoreCase("userassignment")){
			lastTimeChecked = lastTimeUpdateUserAssignments;
		} else if (entity.equalsIgnoreCase("operation")){
			lastTimeChecked = lastTimeUpdateOperations;
		}
		return ((lastTimeChecked + 360000000) < new Date().getTime());
	}
	
	public static GforgeEntityManager getInstance(){
	    instance.compareAndSet(null, new GforgeEntityManager());
		return instance.get();
	}
}
