/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.plexus.redback.rbac.AbstractRole;
import org.codehaus.plexus.redback.rbac.Permission;
import org.codehaus.plexus.redback.rbac.Role;
import org.evolvis.GForgeAPIPortTypeProxy;

import de.tarent.plexus.security.config.ConfigReader;
import de.tarent.plexus.security.webservice.HttpAuthGForgeAPIPortTypeProxy;

public class GforgeRole extends AbstractRole{

	private static final Logger log = Logger.getLogger(GforgeResource.class);
	
	private List<String> childRoleNames = new ArrayList<String>();
	private List<Permission> permissions = new ArrayList<Permission>();
	private String description;
	private String name;
	private boolean assignable;
	private boolean permanent;
	
	public GforgeRole(){}
	
	public GforgeRole(String name, String description){
		this.name = name;
		this.description = description;
	}
	
	public void addChildRoleName(String childRoleName) {
		this.childRoleNames.add(childRoleName);
	}

	public void addPermission(Permission permission) {
		this.permissions.add(permission);
	}

	public List<String> getChildRoleNames() {
		return this.childRoleNames;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public List<Permission> getPermissions() {
		return this.permissions;
	}

	public boolean isAssignable() {
		return assignable;
	}

	public boolean isPermanent() {
		return permanent;
	}

	public void removePermission(Permission permission) {
		this.permissions.remove(permission);
	}

	public void setAssignable(boolean assignable) {
		this.assignable = assignable;
	}

	public void setChildRoleNames(List childRoleNames) {
		this.childRoleNames = childRoleNames;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setName(String name) {
		this.name = name;	
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	public void setPermissions(List permissions) {
		this.permissions = permissions;
	}

	public static Hashtable<String, Role> initializeAll(){
		GforgeEntityManager manager = GforgeEntityManager.getInstance();
		Hashtable<String, Role> data = new Hashtable<String, Role>();
		
		/**************************************************************
		 * 			Create general Roles and set Permissions		  *
		 **************************************************************/
		Role continuumGuest = new GforgeRole("Continuum Guest", "");
		continuumGuest.addPermission(manager.getPermission("Continuum Guest Permission"));
		
		Role guest = new GforgeRole("Guest", "");
		guest.addPermission(manager.getPermission("Guest Permission"));
		
		Role registeredUser = new GforgeRole("Registered User", "");
		registeredUser.addPermission(manager.getPermission("Edit User Data by Username"));
		
		Role systemAdministrator = new GforgeRole("System Administrator", "");
		systemAdministrator.addPermission(manager.getPermission("Edit Redback Configuration"));
		systemAdministrator.addPermission(manager.getPermission("User RBAC Management"));
		
		Role userAdministrator = new GforgeRole("User Administrator", "");
		userAdministrator.addPermission(manager.getPermission("Drop Roles for Anyone"));
		userAdministrator.addPermission(manager.getPermission("Grant Roles for Anyone"));
		userAdministrator.addPermission(manager.getPermission("Create Users"));
		userAdministrator.addPermission(manager.getPermission("Delete Users"));
		userAdministrator.addPermission(manager.getPermission("Edit Users"));
		userAdministrator.addPermission(manager.getPermission("Access Users Roles"));
		userAdministrator.addPermission(manager.getPermission("Access User List"));
		
		Role continuumGroupProjectUser = new GforgeRole("Continuum Group Project User", "");
		
		Role continuumGroupProjectDeveloper = new GforgeRole("Continuum Group Project Developer", "");
		
		Role continuumSystemAdministrator = new GforgeRole("Continuum System Administrator", "");
		continuumSystemAdministrator.addPermission(manager.getPermission("Manage Continuum Configuration"));
		continuumSystemAdministrator.addPermission(manager.getPermission("Manage Continuum Schedules"));
		
		Role continuumGroupProjectAdministrator = new GforgeRole("Continuum Group Project Administrator", "");
		continuumGroupProjectAdministrator.addPermission(manager.getPermission("Add Group to Continuum"));
		continuumGroupProjectAdministrator.addPermission(manager.getPermission("Manage Continuum Schedules"));
		
		Role continuumUserAdministrator = new GforgeRole("Continuum User Administrator", "");
		continuumUserAdministrator.addPermission(manager.getPermission("Manage Continuum Users"));
		
		/**************************************************************
		 * 				set childroles for general roles			  *
		 **************************************************************/
		systemAdministrator.addChildRoleName(userAdministrator.getName());
		guest.addChildRoleName(continuumGuest.getName());
		continuumSystemAdministrator.addChildRoleName(continuumGroupProjectAdministrator.getName());
		systemAdministrator.addChildRoleName(continuumSystemAdministrator.getName());
		userAdministrator.addChildRoleName(continuumUserAdministrator.getName());
		
		/**************************************************************
		 * 				put general roles into hashtable			  *
		 **************************************************************/
		data.put(continuumGuest.getName(), continuumGuest);
		data.put(guest.getName(), guest);
		data.put(registeredUser.getName(), registeredUser);
		data.put(systemAdministrator.getName(), systemAdministrator);
		data.put(userAdministrator.getName(), userAdministrator);
		data.put(continuumGroupProjectUser.getName(), continuumGroupProjectUser);
		data.put(continuumGroupProjectDeveloper.getName(), continuumGroupProjectDeveloper);
		data.put(continuumSystemAdministrator.getName(), continuumSystemAdministrator);
		data.put(continuumGroupProjectAdministrator.getName(), continuumGroupProjectAdministrator);
		data.put(continuumUserAdministrator.getName(), continuumUserAdministrator);
		
		GForgeAPIPortTypeProxy proxy = new HttpAuthGForgeAPIPortTypeProxy();
		proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
		try {
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			String[] projects = proxy.getPublicProjectNames(session);
			for (String project : projects){
				/****************************************************************************
				 * 			Create roles for each project and set permissions				*
				 ****************************************************************************/
				
				Role projectUserProject = new GforgeRole("Project User - " + project, "");
				projectUserProject.addPermission(manager.getPermission("Continuum View Group - " + project));
				
				Role projectDeveloperProject = new GforgeRole("Project Developer - " + project, "");
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Build Group - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Remove Group - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Modify Group - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Add Group Build Definition - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Remove Group Build Definition - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Modify Group Build Definition - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Add Group Notifier - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Remove Group Notifier - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Modify Group Notifier - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Add Project Build Definition - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Remove Project Build Definition - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Modify Project Build Definition - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Add Project Notifer - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Remove Project Notifer - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Modify Project Notifer - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Build Project in Group - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Add Project To Group - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Remove Project from Group - " + project));
				projectDeveloperProject.addPermission(manager.getPermission("Continuum Modify Project in Group - " + project));
				
				Role projectAdministratorProject = new GforgeRole("Project Administrator - " + project, "");
				projectAdministratorProject.addPermission(manager.getPermission("Continuum Manage Users - *"));
				projectAdministratorProject.addPermission(manager.getPermission("Continuum Manage User Roles - *"));
				projectAdministratorProject.addPermission(manager.getPermission("Continuum Grant Group Roles - " + project));
				
				/**************************************************************
				 * 				set childroles for every project			  *
				 **************************************************************/
				continuumGroupProjectUser.addChildRoleName(projectUserProject.getName());
				projectDeveloperProject.addChildRoleName(projectUserProject.getName());
				continuumGroupProjectDeveloper.addChildRoleName(projectDeveloperProject.getName());
				projectAdministratorProject.addChildRoleName(projectDeveloperProject.getName());
				continuumGroupProjectAdministrator.addChildRoleName(projectAdministratorProject.getName());
				
				/**************************************************************
				 * 				put projectroles into hashtable				  *
				 **************************************************************/
				data.put(projectUserProject.getName(), projectUserProject);
				data.put(projectDeveloperProject.getName(), projectDeveloperProject);
				data.put(projectAdministratorProject.getName(), projectAdministratorProject);
				
			}
			proxy.logout(session);
		} catch (RemoteException e) {
			log.error("failed to init GforgePermission (some issue with the soap service?)", e);
		}
		
		return data;
	}
}
