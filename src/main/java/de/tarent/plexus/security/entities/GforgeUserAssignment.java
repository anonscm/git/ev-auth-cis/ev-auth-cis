/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.plexus.redback.rbac.AbstractUserAssignment;
import org.codehaus.plexus.redback.rbac.UserAssignment;
import org.evolvis.GForgeAPIPortTypeProxy;
import org.evolvis.Group;
import org.evolvis.ProjectGroup;

import de.tarent.plexus.security.config.ConfigReader;
import de.tarent.plexus.security.webservice.HttpAuthGForgeAPIPortTypeProxy;


public class GforgeUserAssignment extends AbstractUserAssignment{

	private static final Logger log = Logger.getLogger(GforgeUserAssignment.class);
	
	private String principal;
	private List<String> roleNames = new ArrayList<String>();
	private boolean permanent;
	
	public GforgeUserAssignment(){
		this.roleNames = new ArrayList<String>();
	}
	
	public GforgeUserAssignment(String principal){
		this();
		this.principal = principal;
	}
	
	public String getPrincipal() {
		return this.principal;
	}

	public List getRoleNames() {
		return this.roleNames;
	}

	public boolean isPermanent() {
		return this.permanent;
	}

	public void setPermanent(boolean permanent) {
		this.permanent = permanent;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public void setRoleNames(List roleNames) {
		this.roleNames = roleNames;
	}
	
	@Override
    public void addRoleName(String role) {
		if(!this.roleNames.contains(role)){
			this.roleNames.add(role);
		}
	}

	public static Hashtable<String, UserAssignment> initializeAll(){
		Hashtable<String, UserAssignment> data = new Hashtable<String, UserAssignment>();
		GforgeEntityManager manager = GforgeEntityManager.getInstance();
		
		GForgeAPIPortTypeProxy proxy = new HttpAuthGForgeAPIPortTypeProxy();
		proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
		try {
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			
			String[] projects = proxy.getPublicProjectNames(session);
			for (String project : projects) {
				Group[] groups = proxy.getGroupsByName(session, new String[]{project});
				for (Group group : groups) {
					ProjectGroup[] projectGroups = proxy.getProjectGroups(session, group.getGroup_id());
					for (ProjectGroup projectGroup : projectGroups) {
						org.evolvis.User[] users = proxy.getProjectTechnicians(session, group.getGroup_id(), projectGroup.getGroup_project_id());
						for (org.evolvis.User user : users){
							UserAssignment ua = data.get(user.getUser_name());
							if(ua == null){
								ua = new GforgeUserAssignment(user.getUser_name());
							}
							ua.addRoleName(manager.getRoleByName("Project Administrator - "+ project).getName());
							data.put(ua.getPrincipal(), ua);
						}
					}
				}
			}
			
			// Create UserAssignments for all admins
			log.trace("config admins = " + ConfigReader.getInstance().getAdmins());
			for(String admin : ConfigReader.getInstance().getAdmins()){
				log.info("creating UserAssignments for admin = " + admin );
				data.put(admin, new GforgeUserAssignment(admin));
				data.get(admin).addRoleName("System Administrator");
			}
			
			/************************************************************
			 *   		 		add guest UserAssignment				*
			 ************************************************************/
			UserAssignment guestUa = new GforgeUserAssignment("guest");
			guestUa.addRoleName("Guest");
			data.put("guest", guestUa);
			proxy.logout(session);
		} catch (RemoteException e) {
			log.error("failed to init GforgeUserAssignment (some issue with the soap service?)", e);
		}
		
		return data;
	}
}
