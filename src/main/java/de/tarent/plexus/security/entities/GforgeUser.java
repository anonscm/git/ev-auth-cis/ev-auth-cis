/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.plexus.redback.users.User;

public class GforgeUser implements User{

	private int userId; 
	private String mail;
	private String userName;
	private String fullName;
	
	public GforgeUser(){
		super();
	}
	
	public GforgeUser(int userId, String mail, String userName, String fullName){
		this.userId = userId;
		this.mail = mail;
		this.userName = userName;
		this.fullName = fullName;
	}
	
	public void addPreviousEncodedPassword(String arg0) {}

	public Date getAccountCreationDate() {
		return new Date();
	}

	public int getCountFailedLoginAttempts() {
		return 0;
	}

	public String getEmail() {
		return this.mail;
	}

	public String getEncodedPassword() {
		return "";
	}

	public String getFullName() {
		return this.fullName;
	}

	public Date getLastLoginDate() {
		return new Date();
	}

	public Date getLastPasswordChange() {
		return new Date();
	}

	public String getPassword() {
		return "";
	}

	public List getPreviousEncodedPasswords() {
		return new ArrayList();
	}

	public Object getPrincipal() {
		return this.userName;
	}

	public String getUsername() {
		return this.userName;
	}

	public boolean isLocked() {
		return false;
	}

	public boolean isPasswordChangeRequired() {
		return false;
	}

	public boolean isPermanent() {
		return false;
	}

	public boolean isValidated() {
		return false;
	}

	public void setCountFailedLoginAttempts(int arg0) {	}

	public void setEmail(String mail) {
		this.mail = mail;
	}

	public void setEncodedPassword(String arg0) {
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setLastLoginDate(Date arg0) {}

	public void setLastPasswordChange(Date arg0) {}

	public void setLocked(boolean arg0) {}

	public void setPassword(String arg0) {}

	public void setPasswordChangeRequired(boolean arg0) {}

	public void setPermanent(boolean arg0) {}

	public void setPreviousEncodedPasswords(List arg0) {}

	public void setUsername(String username) {
		this.userName = username;
	}

	public void setValidated(boolean arg0) {}
	
	public int getUserId(){
		return this.userId;
	}
	
	public void setUserId(int userId){
		this.userId = userId;
	}

	@Override
	public boolean equals(Object obj) {
		try{
			if( ((GforgeUser)obj).getUserId() == this.userId){
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

}
