/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.gforge.rbac;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;
import org.codehaus.plexus.personality.plexus.lifecycle.phase.Initializable;
import org.codehaus.plexus.redback.rbac.AbstractRBACManager;
import org.codehaus.plexus.redback.rbac.Operation;
import org.codehaus.plexus.redback.rbac.Permission;
import org.codehaus.plexus.redback.rbac.RBACManager;
import org.codehaus.plexus.redback.rbac.RBACManagerListener;
import org.codehaus.plexus.redback.rbac.RbacManagerException;
import org.codehaus.plexus.redback.rbac.RbacObjectInvalidException;
import org.codehaus.plexus.redback.rbac.RbacObjectNotFoundException;
import org.codehaus.plexus.redback.rbac.Resource;
import org.codehaus.plexus.redback.rbac.Role;
import org.codehaus.plexus.redback.rbac.UserAssignment;

import de.tarent.plexus.security.entities.GforgeEntityManager;
import de.tarent.plexus.security.entities.GforgeOperation;
import de.tarent.plexus.security.entities.GforgePermission;
import de.tarent.plexus.security.entities.GforgeResource;
import de.tarent.plexus.security.entities.GforgeRole;
import de.tarent.plexus.security.entities.GforgeUserAssignment;

public class GforgeRbacManager extends AbstractRBACManager
	implements RBACManagerListener, Initializable {

    private static final Logger log = Logger.getLogger(GforgeRbacManager.class);
    
	private static final AtomicReference<RBACManager> instance = new AtomicReference<RBACManager>();
	
	private final Hashtable<String, Role> roles = new Hashtable<String, Role>();
	private final Hashtable<String, Permission> permissions = new Hashtable<String, Permission>();
	private final Hashtable<String, Operation> operations = new Hashtable<String, Operation>();
	private final Hashtable<String, UserAssignment> userAssignments = new Hashtable<String, UserAssignment>();
	
	// WARNING: do not hide constr. - this class get's initialized as a spring bean ...
	//private GforgeRbacManager() {
	//    // hide constr.
	//}
	
	public void rbacInit(boolean freshdb) {
	    if (log.isDebugEnabled()) {
		    log.debug("rbacInit("+freshdb+")");
	    }
		fireRbacInit(freshdb);		
	}

	public void rbacPermissionRemoved(Permission arg0) {}

	public void rbacPermissionSaved(Permission arg0) {}

	public void rbacRoleRemoved(Role arg0) {}

	public void rbacRoleSaved(Role arg0) {	}

	public void rbacUserAssignmentRemoved(UserAssignment arg0) {}

	public void rbacUserAssignmentSaved(UserAssignment arg0) {	}

	public Operation createOperation(String name) throws RbacManagerException {
	    log.trace("createOperation(...)");
		try {
			return getOperation(name);
		} catch (RbacObjectNotFoundException e) {
			return new GforgeOperation(name, "");
			
		} catch (RbacManagerException e) {
			return new GforgeOperation(name, "");
		}
	}

	public Permission createPermission(String name, String operation, String resource) throws RbacManagerException {
	    log.trace("createPermission(...)");
		GforgeEntityManager manager = GforgeEntityManager.getInstance();
		try {
			return getPermission(name);
		} catch (RbacObjectNotFoundException e) {
			return new GforgePermission(name, "", manager.getOperation(operation), manager.getResource(resource));
			
		} catch (RbacManagerException e) {
			return new GforgePermission(name, "", manager.getOperation(operation), manager.getResource(resource));
		}
	}

	public Permission createPermission(String name) throws RbacManagerException {
	    log.trace("createPermission(...)");
		try {
			return getPermission(name);
		} catch (RbacObjectNotFoundException e) {
			return new GforgePermission(name, "", null, null);
			
		} catch (RbacManagerException e) {
			return new GforgePermission(name, "", null, null);
		}
	}

	public Resource createResource(String name) throws RbacManagerException {
	    log.trace("createResource(...)");
		try {
			return getResource(name);
		} catch (RbacObjectNotFoundException e) {
			return new GforgeResource(name);
			
		} catch (RbacManagerException e) {
			return new GforgeResource(name);
		}
	}

	public Role createRole(String roleName) {
	    log.trace("createRole(...)");
		Role role = null;
		try {
			return getRole(roleName);
		} catch (RbacObjectNotFoundException e) {
			role = new GforgeRole();
			role.setName(roleName);
			return role;
		} catch (RbacManagerException e) {
			role = new GforgeRole();
			role.setName(roleName);
			return role;
		}
	}

	public UserAssignment createUserAssignment(String principal) throws RbacManagerException {
	    log.trace("createUserAssignment(...)");
		UserAssignment ua = getUserAssignment(principal);
		if(ua == null){
			ua = new GforgeUserAssignment();
			ua.setPrincipal(principal);
		}
		return ua;
	}

	public void eraseDatabase() {}

	public List getAllOperations() throws RbacManagerException {
	    log.trace("getAllOperations()");
		List list = new ArrayList();
		for(Enumeration e = GforgeEntityManager.getInstance().getAllOperations().elements(); e.hasMoreElements();){
			list.add(e.nextElement());
		}
		return list;
	}

	public List getAllPermissions() throws RbacManagerException {
	    log.trace("getAllPermissions()");
		List list = new ArrayList();
		for(Enumeration e = GforgeEntityManager.getInstance().getAllPermissions().elements(); e.hasMoreElements();){
			list.add(e.nextElement());
		}
		return list;
	}

	public List getAllResources() throws RbacManagerException {
	    log.trace("getAllResources()");
		List list = new ArrayList();
		for(Enumeration e = GforgeEntityManager.getInstance().getAllResources().elements(); e.hasMoreElements();){
			list.add(e.nextElement());
		}
		return list;
	}

	public List getAllRoles() throws RbacManagerException {
	    log.trace("getAllRoles()");
		List list = new ArrayList();
		for(Enumeration e = GforgeEntityManager.getInstance().getAllRoles().elements(); e.hasMoreElements();){
			list.add(e.nextElement());
		}
		return list;
	}

	public List getAllUserAssignments() throws RbacManagerException {
	    log.trace("getAllUserAssignments()");
		List list = new ArrayList();
		for(Enumeration e = GforgeEntityManager.getInstance().getAlluserAssignments().elements(); e.hasMoreElements();){
			list.add(e.nextElement());
		}
		return list;
	}

	public Operation getOperation(String name) throws RbacObjectNotFoundException, RbacManagerException {
	    if (log.isTraceEnabled()) {
		    log.trace("getOperation(" + name + ")");
	    }
		return GforgeEntityManager.getInstance().getOperation(name);
	}

	public Permission getPermission(String name) throws RbacObjectNotFoundException, RbacManagerException {
	    if (log.isTraceEnabled()) {
		    log.trace("getPermission(" + name + ")");
	    }
		return GforgeEntityManager.getInstance().getPermission(name);
	}

	public Resource getResource(String name) throws RbacObjectNotFoundException, RbacManagerException {
	    if (log.isTraceEnabled()) {
		    log.trace("getResource(" + name + ")");
	    }
		return GforgeEntityManager.getInstance().getResource(name);
	}

	public Role getRole(String name) throws RbacObjectNotFoundException, RbacManagerException {
	    if (log.isTraceEnabled()) {
		    log.trace("getRole(" + name + ")");
	    }
		return GforgeEntityManager.getInstance().getRoleByName(name);
	}

	public UserAssignment getUserAssignment(String name) throws RbacObjectNotFoundException, RbacManagerException {
	    if (log.isTraceEnabled()) {
		    log.trace("getUserAssignment("+name+")");
	    }
		String tempname = name.replaceAll("\\n", "");
		UserAssignment ua = GforgeEntityManager.getInstance().getUserassignment(tempname);
		return ua;
	}

	public List getUserAssignmentsForRoles(Collection roleNames) throws RbacManagerException {
	    if (log.isTraceEnabled()) {
            log.trace("getUserAssignmentForRoles("+roleNames+")");
        }
		List list = new ArrayList<UserAssignment>();
		Enumeration<UserAssignment> userAssignments = GforgeEntityManager.getInstance().getAlluserAssignments().elements();
		while(userAssignments.hasMoreElements()){
			UserAssignment ua = userAssignments.nextElement();
			for (String roleName : (List<String>)(ua.getRoleNames())) {
				if(roleNames.contains(roleName)){
					list.add(ua);
					break;
				}
			}
		}
		return list;
	}

	public void removeOperation(Operation arg0) throws RbacObjectNotFoundException, RbacObjectInvalidException, RbacManagerException {}

	public void removePermission(Permission arg0) throws RbacObjectNotFoundException, RbacObjectInvalidException, RbacManagerException {}

	public void removeResource(Resource arg0) throws RbacObjectNotFoundException, RbacObjectInvalidException, RbacManagerException {}

	public void removeRole(Role arg0) throws RbacObjectNotFoundException, RbacObjectInvalidException, RbacManagerException {}

	public void removeUserAssignment(UserAssignment arg0) throws RbacObjectNotFoundException, RbacObjectInvalidException, RbacManagerException {}

	public Operation saveOperation(Operation operation) throws RbacObjectInvalidException, RbacManagerException {
		return operation;
	}

	public Permission savePermission(Permission permission) throws RbacObjectInvalidException, RbacManagerException {
		return permission;
	}

	public Resource saveResource(Resource resource) throws RbacObjectInvalidException, RbacManagerException {	
		return resource;
	}

	public Role saveRole(Role role) throws RbacObjectInvalidException, RbacManagerException {
		this.roles.put(role.getName(), role);
		return role;
	}

	public void saveRoles(Collection roles) throws RbacObjectInvalidException, RbacManagerException {
		for (Role role : (Collection<Role>)(roles)) {
			this.roles.put(role.getName(), role);
		}
	}

	public UserAssignment saveUserAssignment(UserAssignment userAssignment) throws RbacObjectInvalidException, RbacManagerException {
		this.userAssignments.put(userAssignment.getPrincipal(), userAssignment);
		return userAssignment;
	}
	
	public static RBACManager getInstance(){
	    instance.compareAndSet(null, new GforgeRbacManager());
		return instance.get();
	}    
}
