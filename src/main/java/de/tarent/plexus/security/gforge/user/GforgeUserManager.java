/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.gforge.user;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.plexus.redback.users.AbstractUserManager;
import org.codehaus.plexus.redback.users.User;
import org.codehaus.plexus.redback.users.UserManager;
import org.codehaus.plexus.redback.users.UserNotFoundException;
import org.codehaus.plexus.redback.users.UserQuery;
import org.evolvis.GForgeAPIPortTypeProxy;
import org.evolvis.Group;
import org.evolvis.ProjectGroup;

import de.tarent.plexus.security.config.ConfigReader;
import de.tarent.plexus.security.entities.GforgeUser;
import de.tarent.plexus.security.webservice.HttpAuthGForgeAPIPortTypeProxy;

public class GforgeUserManager extends AbstractUserManager implements UserManager {

    private static final Logger log = Logger.getLogger(GforgeUserManager.class);
    
	private static GForgeAPIPortTypeProxy proxy; 
	private List<GforgeUser> userList = null;
	private long lastTimeChecked = -1;
	
	public User addUser(User user) {
		return user;
	}

	public void addUserUnchecked(User arg0) {
	}

	public User createUser(String arg0, String arg1, String arg2) {
		return null;
	}

	public UserQuery createUserQuery() {
		return null;
	}

	public void deleteUser(Object arg0) throws UserNotFoundException {}

	public void deleteUser(String arg0) throws UserNotFoundException {}

	public void eraseDatabase() {}

	public User findUser(Object principal) throws UserNotFoundException {
		return findUser((String)principal);
	}

	public User findUser(String userName) throws UserNotFoundException {
		if(userName.equalsIgnoreCase("guest")){
			return new GforgeUser(-1, "guest@example.org", "guest", "guest");
		}
		return (User)(findUsersByUsernameKey(userName, true).get(0));
		
	}

	public List findUsersByEmailKey(String arg0, boolean arg1) {
		List<GforgeUser> users = new ArrayList<GforgeUser>();
		if( checkIfUsersShouldBeUpdatet() ){
			loadAllUsers();
		}
		for(GforgeUser user : userList){
			if(user.getFullName().equals(arg0))
				users.add(user);
		}
		return users;
	}

	public List findUsersByFullNameKey(String arg0, boolean arg1) {
		List<GforgeUser> users = new ArrayList<GforgeUser>();
		if( checkIfUsersShouldBeUpdatet() ){
			loadAllUsers();
		}
		for(GforgeUser user : userList){
			if(user.getFullName().equals(arg0))
				users.add(user);
		}
		return users;
	}

	public List findUsersByQuery(UserQuery arg0) {
		return null;
	}

	public List findUsersByUsernameKey(String arg0, boolean arg1) {
		try{
			List<GforgeUser> userList = new ArrayList<GforgeUser>();
			GForgeAPIPortTypeProxy proxy = getProxy();
			proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			org.evolvis.User[] users = proxy.getUsersByName(session, new String[] {arg0});
			for(org.evolvis.User user : users){
				userList.add(new GforgeUser(user.getUser_id(), user.getUser_name(), user.getUser_name(), user.getFirstname() + " " + user.getLastname()));
			}
			return userList;
		} catch (RemoteException e) {
		    log.error("failed to get user by username '" + arg0 + "'", e);
			return new ArrayList<GforgeUser>();
		}
	}

	public String getId() {
		return GforgeUserManager.class.getName();
	}

	public List getUsers() {
		if( checkIfUsersShouldBeUpdatet() ) {
			loadAllUsers();
		}
		return this.userList;
	}
	
	public void loadAllUsers() {
		List<GforgeUser> userList = new ArrayList<GforgeUser>(); 
		try {
			GForgeAPIPortTypeProxy proxy = getProxy();
			proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			String[] projects = proxy.getPublicProjectNames(session);
			for (String project : projects) {
				Group[] groups = proxy.getGroupsByName(session, new String[]{project});
				for (Group group : groups) {
					ProjectGroup[] projectGroups = proxy.getProjectGroups(session, group.getGroup_id());
					for (ProjectGroup projectGroup : projectGroups) {
						org.evolvis.User[] users = proxy.getProjectTechnicians(session, group.getGroup_id(), projectGroup.getGroup_project_id());
						for (org.evolvis.User user : users){
							GforgeUser gfUser = new GforgeUser(user.getUser_id(), user.getUser_name(), user.getUser_name(), user.getFirstname() + " " + user.getLastname());
							if(!userList.contains(gfUser)){
								userList.add(gfUser);
							}
						}
					}
				}
			}
			proxy.logout("session");
			this.userList = userList;
			this.lastTimeChecked = new Date().getTime();
		} catch (RemoteException e) {
		    log.error("failed to fetch users from webservice", e);
		}
	}

	public List getUsers(boolean sort) {
		return getUsers();
	}

	public User updateUser(User user) throws UserNotFoundException {
		return user;
	}

	public boolean userExists(Object principal) {
		try {
			GForgeAPIPortTypeProxy proxy = getProxy();
			proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
			String session = proxy.login(ConfigReader.getInstance().getConfig("user"), ConfigReader.getInstance().getConfig("pwd"));
			org.evolvis.User[] users = proxy.getUsersByName(session, new String[]{(String)principal});
			proxy.logout("session");
			if(users.length == 1){
				return true;
			} 
			return false;
		} catch (RemoteException e) {
		    log.error("failed to check if user exists for principal = " + principal, e);
			return false;
		}
	}
	
	private GForgeAPIPortTypeProxy getProxy() {
		if (proxy == null) {
			proxy = new HttpAuthGForgeAPIPortTypeProxy();
			proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
		}
		return proxy;
	}
	
	public boolean checkIfUsersShouldBeUpdatet(){
	     // TODO: System.currentTimeMillis() ?
		return ((this.lastTimeChecked + 360000) < new Date().getTime()
		|| this.userList == null 
		|| this.userList.size() == 0);
	}
	
	public boolean isReadOnly(){
		return true;
	}
}
