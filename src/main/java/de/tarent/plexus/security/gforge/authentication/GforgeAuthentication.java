/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.gforge.authentication;

import org.apache.log4j.Logger;
import org.codehaus.plexus.redback.authentication.AuthenticationDataSource;
import org.codehaus.plexus.redback.authentication.AuthenticationException;
import org.codehaus.plexus.redback.authentication.AuthenticationResult;
import org.codehaus.plexus.redback.authentication.Authenticator;
import org.codehaus.plexus.redback.authentication.PasswordBasedAuthenticationDataSource;
import org.codehaus.plexus.redback.policy.AccountLockedException;
import org.codehaus.plexus.redback.users.UserManager;
import org.evolvis.GForgeAPIPortTypeProxy;

import de.tarent.plexus.security.config.ConfigReader;
import de.tarent.plexus.security.webservice.HttpAuthGForgeAPIPortTypeProxy;

public class GforgeAuthentication implements Authenticator {
	
	 	private static final Logger log = Logger.getLogger(GforgeAuthentication.class);

    	/**
	    * @plexus.requirement
	    */	
	    UserManager manager;
		
	    public static final String ROLE = GforgeAuthentication.class.getName();

	    public String getId(){
	    	return GforgeAuthentication.class.getName();
	    }

	    public boolean supportsDataSource( AuthenticationDataSource source ){
	    	return true;
	    }

	    public AuthenticationResult authenticate( AuthenticationDataSource source )
	        throws AccountLockedException, AuthenticationException{
	    	
	    	log.debug("authenticate("+source+")");
	    	
			GForgeAPIPortTypeProxy proxy = new HttpAuthGForgeAPIPortTypeProxy();
			proxy.setEndpoint(ConfigReader.getInstance().getConfig("GFUrl"));
			
			if (source instanceof PasswordBasedAuthenticationDataSource) {
		
				PasswordBasedAuthenticationDataSource src = (PasswordBasedAuthenticationDataSource)source;
				
				String srcUName = src.getPrincipal();
				String srcPwd = src.getPassword();
				
				try {
					String session = proxy.login(srcUName, srcPwd);
					if ( session != null ) {
						proxy.logout(session);
						log.debug("authenticate(...) - success");
						return new AuthenticationResult(true, srcUName, null);
					} 
					log.debug("authenticate(...) - fail: failed to create SOAP session");
					return new AuthenticationResult(false, srcUName, null);
					
				} catch (Exception e) {
					log.debug("authenticate(...) - fail: ",e );
					return new AuthenticationResult(false, srcUName, e);
				}
			}
			log.debug("authenticate(...) - fail: unkown authSource class ...");
			return new AuthenticationResult(false, source.getPrincipal(), null);
			
	    }
}
