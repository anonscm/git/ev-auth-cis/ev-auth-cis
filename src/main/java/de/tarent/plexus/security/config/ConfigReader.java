/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.log4j.Logger;

public class ConfigReader {

    private static final String CONF_FILENAME = "/conf/gforge.cnf";

    public static final String ENV_PLEXUS_HOME = "PLEXUS_HOME";

    private static final Logger log = Logger.getLogger(ConfigReader.class);

    private static final AtomicReference<ConfigReader> instance =
            new AtomicReference<ConfigReader>();

    private final Map<String, String> configs;

    private ConfigReader() {
        this.configs = new Hashtable<String, String>();
        this.initializeConfig();
    }

    private void initializeConfig() {

        try {

            final BufferedReader reader = mkConfigReader();

            int tmp = -1;
            StringBuffer config = new StringBuffer();

            while ((tmp = reader.read()) != -1) {
                config.append((char) tmp);
            }

            String[] parts = config.toString().split("\n");
            log.trace("Config-Parts = " + parts);
            log.trace("Config-Parts-length = " + parts.length);
            log.trace("Config " + config);
            for (int i = 0; i < parts.length; i++) {
                String part = parts[i];
                String[] configParts = part.split("=");
                log.trace("ConfigName " + configParts[0]);
                if (configParts.length == 2) {
                    configs.put(configParts[0], configParts[1]);
                }
            }
            log.debug("Config-Parts-Table " + this.configs);
        } catch (FileNotFoundException e) {
            log.error("Error: Config konnte nicht eingelesen werden!", e);
        } catch (IOException e) {
            log.error("Error: Config konnte nicht eingelesen werden!", e);
        }
    }

    private BufferedReader mkConfigReader() throws FileNotFoundException {
        String cfgPath = System.getenv(ENV_PLEXUS_HOME);
        if (cfgPath == null) {
            cfgPath = System.getProperty(ENV_PLEXUS_HOME);
        }
        final BufferedReader reader;

        if (cfgPath == null) {
            try {
                log.info("no " + ENV_PLEXUS_HOME
                        + " set - trying to read resource /"
                        + CONF_FILENAME);
                reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/" + CONF_FILENAME)));
            } catch (RuntimeException e) {
                log.warn("WARNING: You should set the '"
                        + ENV_PLEXUS_HOME
                        + "' enviroment variable or system property - both are null and no "
                        + "resource is present either");
                throw e;
            }
            
        } else {
            log.info("reading config from " + cfgPath + CONF_FILENAME);
            reader =
                    new BufferedReader(new FileReader(new File(cfgPath
                            + CONF_FILENAME)));
        }
        return reader;
    }

    public static ConfigReader getInstance() {
        // check & re-check cc-pattern to avoid re-reading the config again and
        // again ...
        if (instance.get() != null) {
            return instance.get();
        }
        instance.compareAndSet(null, new ConfigReader());
        return instance.get();
    }

    public String getConfig(String name) {
        return this.configs.get(name);
    }

    public boolean hasConfig(String name) {
        return this.configs.containsKey(name);
    }

    public List<String> getAdmins() {
        List<String> admins = new ArrayList<String>();
        String adminNames = null;
        if ((adminNames = this.getConfig("admins")) != null) {
            String[] names = adminNames.split(",");
            for (String name : names) {
                log.debug("admin " + name);
                admins.add(name);
            }
        }
        return admins;
    }

}
