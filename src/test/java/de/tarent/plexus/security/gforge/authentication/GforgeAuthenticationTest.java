/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.plexus.security.gforge.authentication;

import static org.junit.Assert.*;

import org.codehaus.plexus.redback.authentication.AuthenticationException;
import org.codehaus.plexus.redback.authentication.AuthenticationResult;
import org.codehaus.plexus.redback.authentication.PasswordBasedAuthenticationDataSource;
import org.codehaus.plexus.redback.authentication.TokenBasedAuthenticationDataSource;
import org.codehaus.plexus.redback.policy.AccountLockedException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Fabian Bieker
 *
 */
public class GforgeAuthenticationTest {

    private GforgeAuthentication auth;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        auth = new GforgeAuthentication();
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.authentication.GforgeAuthentication#getId()}.
     */
    @Test
    public void testGetId() {
        assertEquals(GforgeAuthentication.class.getName(), auth.getId());
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.authentication.GforgeAuthentication#supportsDataSource(org.codehaus.plexus.redback.authentication.AuthenticationDataSource)}.
     */
    @Test
    public void testSupportsDataSource() {
        // TODO: that's a little odd
        //assertTrue(auth.supportsDataSource(null));
        
        assertTrue(auth.supportsDataSource(new PasswordBasedAuthenticationDataSource()));
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.authentication.GforgeAuthentication#authenticate(org.codehaus.plexus.redback.authentication.AuthenticationDataSource)}.
     */
    @Test
    @Ignore
    public void testAuthenticate() throws AccountLockedException, AuthenticationException {
        AuthenticationResult res = auth.authenticate(null);
        assertNotNull(res);
        assertFalse(res.isAuthenticated());
        assertNull(res.getPrincipal());
        
        // do not authenticate empty data source
        res = auth.authenticate(new PasswordBasedAuthenticationDataSource());
        assertNotNull(res);
        assertFalse(res.isAuthenticated());
        assertNull(res.getPrincipal());
        
        // do not use anything else than password based auth
        res = auth.authenticate(new TokenBasedAuthenticationDataSource());
        assertNotNull(res);
        assertFalse(res.isAuthenticated());
        assertNull(res.getPrincipal());
        
        PasswordBasedAuthenticationDataSource src = new PasswordBasedAuthenticationDataSource();
        src.setPrincipal("username");
        src.setPassword("pass");
        res = auth.authenticate(src);
        assertNotNull(res);
        assertEquals(src.getPrincipal(), res.getPrincipal());
        //assertFalse(res.isAuthenticated());
        
        // TODO: use a mock webservice to auth against ...
    }

}
