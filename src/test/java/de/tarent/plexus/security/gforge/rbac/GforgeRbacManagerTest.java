/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.plexus.security.gforge.rbac;

import static org.junit.Assert.*;

import org.codehaus.plexus.redback.rbac.RBACManager;
import org.codehaus.plexus.redback.rbac.RbacManagerException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author Fabian Bieker
 *
 */
public class GforgeRbacManagerTest {

    private GforgeRbacManager rm;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        rm = (GforgeRbacManager) GforgeRbacManager.getInstance();
        assertNotNull(rm);
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#rbacInit(boolean)}.
     */
    @Test
    public void testRbacInit() {
        // just make sure no exception is thrown, just calls ABC anyway ...
        rm.rbacInit(true);
        rm.rbacInit(false);
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#createOperation(java.lang.String)}.
     */
    @Test
    public void testCreateOperation() throws RbacManagerException {
        
        try {
	        rm.createOperation(null);
	        fail("last method call should throw NPE");
        } catch (NullPointerException e) {
            // ok
        }
        
        //rm.c
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#createPermission(java.lang.String, java.lang.String, java.lang.String)}.
     */
    @Test
    @Ignore
    public void testCreatePermissionStringStringString() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#createPermission(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testCreatePermissionString() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#createResource(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testCreateResource() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#createRole(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testCreateRole() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#createUserAssignment(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testCreateUserAssignment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#eraseDatabase()}.
     */
    @Test
    @Ignore
    public void testEraseDatabase() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getAllOperations()}.
     */
    @Test
    @Ignore
    public void testGetAllOperations() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getAllPermissions()}.
     */
    @Test
    @Ignore
    public void testGetAllPermissions() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getAllResources()}.
     */
    @Test
    @Ignore
    public void testGetAllResources() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getAllRoles()}.
     */
    @Test
    @Ignore
    public void testGetAllRoles() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getAllUserAssignments()}.
     */
    @Test
    @Ignore
    public void testGetAllUserAssignments() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getOperation(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testGetOperation() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getPermission(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testGetPermission() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getResource(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testGetResource() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getRole(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testGetRole() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getUserAssignment(java.lang.String)}.
     */
    @Test
    @Ignore
    public void testGetUserAssignment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getUserAssignmentsForRoles(java.util.Collection)}.
     */
    @Test
    @Ignore
    public void testGetUserAssignmentsForRoles() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#removeOperation(org.codehaus.plexus.redback.rbac.Operation)}.
     */
    @Test
    @Ignore
    public void testRemoveOperationOperation() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#removePermission(org.codehaus.plexus.redback.rbac.Permission)}.
     */
    @Test
    @Ignore
    public void testRemovePermissionPermission() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#removeResource(org.codehaus.plexus.redback.rbac.Resource)}.
     */
    @Test
    @Ignore
    public void testRemoveResourceResource() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#removeRole(org.codehaus.plexus.redback.rbac.Role)}.
     */
    @Test
    @Ignore
    public void testRemoveRoleRole() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#removeUserAssignment(org.codehaus.plexus.redback.rbac.UserAssignment)}.
     */
    @Test
    @Ignore
    public void testRemoveUserAssignmentUserAssignment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#saveOperation(org.codehaus.plexus.redback.rbac.Operation)}.
     */
    @Test
    @Ignore
    public void testSaveOperation() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#savePermission(org.codehaus.plexus.redback.rbac.Permission)}.
     */
    @Test
    @Ignore
    public void testSavePermission() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#saveResource(org.codehaus.plexus.redback.rbac.Resource)}.
     */
    @Test
    @Ignore
    public void testSaveResource() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#saveRole(org.codehaus.plexus.redback.rbac.Role)}.
     */
    @Test
    @Ignore
    public void testSaveRole() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#saveRoles(java.util.Collection)}.
     */
    @Test
    @Ignore
    public void testSaveRoles() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#saveUserAssignment(org.codehaus.plexus.redback.rbac.UserAssignment)}.
     */
    @Test
    @Ignore
    public void testSaveUserAssignment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link de.tarent.plexus.security.gforge.rbac.GforgeRbacManager#getInstance()}.
     */
    @Test
    @Ignore
    public void testGetInstance() {
        final RBACManager m = GforgeRbacManager.getInstance();
        assertNotNull(m);
        assertTrue(m == GforgeRbacManager.getInstance()); // yes, ==
    }

}
