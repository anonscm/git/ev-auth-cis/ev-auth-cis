package de.tarent.plexus.security.webservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;

import javax.net.ssl.SSLSocketFactory;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Just some code to reproduce some bugs in the evolvis webservice.
 * Those bugs are fixed in the current evolvis svn trunk ...
 * 
 * @author Fabian Bieker
 * 
 */
@Ignore("not really a unit test ...")
public class TestLdsSoapApi {

	@Test
	public void testGetProjects() throws Exception {
		Socket sock = SSLSocketFactory.getDefault().createSocket(
				InetAddress.getByName("lds-evolvis.tarent.de"), 443);

		BufferedReader in = new BufferedReader(new InputStreamReader(sock
				.getInputStream()));
		PrintStream out = new PrintStream(sock.getOutputStream());

		String soapmsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
				+ "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" "
				+ "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" "
				+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"> "
				+ "<soapenv:Body> "
				+ "<ns1:getProjectGroups soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
				+ "xmlns:ns1=\"http://evolvis.org\"> "
				+ "<session_ser xsi:type=\"xsd:string\">MTQwLSotMTIzNjY4Nzc1Ny0qLTg3LjE1OC4yMy4yMDAtKi1BeGlzLzEuNA==-*-cef981af3a9ae82ecc8fefb5d0c1d366</session_ser> "
				+ "<group_id href=\"#id0\"/> "
				+ "</ns1:getProjectGroups> "
				+ "<multiRef id=\"id0\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
				+ "xsi:type=\"xsd:int\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">10</multiRef> "
				+ "</soapenv:Body> " + "</soapenv:Envelope> ";

		out.println("POST /soap/index.php HTTP/1.0"); // TODO: what's about 1.1?
		out.println("Content-Type: text/xml; charset=utf-8");
		out
				.println("Accept: application/soap+xml, application/dime, multipart/related, text/*.");
		out.println("Host: lds-evolvis.tarent.de");
		out.println("Cache-Control: no-cache");
		// out.println("SOAPAction: \"http://lds-evolvis.tarent.de#getProjectGroups\"");
		out.println("SOAPAction: \"http://evolvis.org#getProjectGroups\"");
		out.println("Content-Length: " + soapmsg.length());
		out.println("Authorization: Basic CHANGE_ME");
		// FIXME: if I disable this header, I get an valid response ...
		out.println("User-Agent: Axis/1.4");
		// out.println("User-Agent: Java/1.5.0.17");
		out.println();

		out.println(soapmsg);
		out.println();
		out.println();

		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}
		System.out.println("----------------------------------------");

		sock.close();
	}

	@Test
	@Ignore("no longer relevant ...")
	public void testGetProjects2() throws Exception {
		Socket sock = SSLSocketFactory.getDefault().createSocket(
				InetAddress.getByName("lds-evolvis.tarent.de"), 443);

		BufferedReader in = new BufferedReader(new InputStreamReader(sock
				.getInputStream()));
		PrintStream out = new PrintStream(sock.getOutputStream());

		String soapmsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "<env:Envelope xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
				+ "xmlns:enc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns0=\"http://evolvis.org\" env:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"> "
				+ "<env:Body> "
				+ "<ns0:getProjectGroups><session_ser xsi:type=\"xsd:string\">MzMxLSotMTIzNjg3MDMyMS0qLTg3LjE1OC4yMy4xMzQtKi1KYXZhLzEuNS4wXzE3-*-aecff3a14c2e140f84d879b870c80e21</session_ser> "
				+ "<group_id xsi:type=\"xsd:int\">10</group_id> "
				+ "</ns0:getProjectGroups> " + "</env:Body> "
				+ "</env:Envelope>";

		out.println("POST /soap/index.php HTTP/1.1");
		out.println("Content-Type: text/xml; charset=utf-8");
		out
				.println("Accept: Accept: text/xml, text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
		out.println("Content-Length: " + soapmsg.length());
		// out.println("SOAPAction: \"http://lds-evolvis.tarent.de#getProjectGroups\"");
		out.println("SOAPAction: \"http://evolvis.org#getProjectGroups\"");
		out.println("Authorization: Basic CHANGE_ME");
		out.println("User-Agent: Java/1.5.0.17");
		out.println("Host: lds-evolvis.tarent.de");
		// out.println("Connection: keep-alive");
		out.println();

		out.println(soapmsg);
		out.println();
		out.println();

		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}
		System.out.println("----------------------------------------");

		sock.close();
	}

	@Test
	public void testGetProjectTechnicans() throws Exception {
		Socket sock = SSLSocketFactory.getDefault().createSocket(
				InetAddress.getByName("lds-evolvis.tarent.de"), 443);

		BufferedReader in = new BufferedReader(new InputStreamReader(sock
				.getInputStream()));
		PrintStream out = new PrintStream(sock.getOutputStream());

		String soapmsg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> "
				+ "<soapenv:Envelope><soapenv:Body>" 
				+ "<ns1:getProjectTechnicians soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"http://evolvis.org\">"
				+ "<session_ser xsi:type=\"xsd:string\">MTQwLSotMTIzNzQ2NjczMi0qLTg3LjE1OC4xNy4yNDAtKi1BeGlzLzEuNCwgZm9vYmFy-*-ce06f07ce30a2c0e34970bce65da6722</session_ser>"
				+ "<group_id href=\"#id0\"/><group_project_id href=\"#id1\"/></ns1:getProjectTechnicians>"
				+ "<multiRef id=\"id1\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
				+ "xsi:type=\"xsd:int\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">6</multiRef> "
				+ "<multiRef id=\"id0\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" "
				+ "xsi:type=\"xsd:int\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\">7</multiRef>"
				+ "</soapenv:Body></soapenv:Envelope>";

		out.println("POST /soap/index.php HTTP/1.0"); // TODO: what's about 1.1?
		out.println("Content-Type: text/xml; charset=utf-8");
		out
				.println("Accept: application/soap+xml, application/dime, multipart/related, text/*.");
		out.println("Host: lds-evolvis.tarent.de");
		out.println("Cache-Control: no-cache");
		out.println("SOAPAction: \"http://evolvis.org#getProjectTechnicians\"");
		out.println("Content-Length: " + soapmsg.length());
		out.println("Authorization: Basic CHANGE_ME");
		out.println("User-Agent: Axis/1.4");
		out.println();

		out.println(soapmsg);
		out.println();
		out.println();

		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}
		System.out.println("----------------------------------------");

		sock.close();
	}

}
