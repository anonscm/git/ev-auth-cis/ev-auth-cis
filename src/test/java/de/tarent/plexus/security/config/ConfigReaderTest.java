/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.plexus.security.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Fabian Bieker
 */
public class ConfigReaderTest {

    @BeforeClass
    public static void setEnv() {
        if (System.getenv(ConfigReader.ENV_PLEXUS_HOME) == null
                && System.getProperty(ConfigReader.ENV_PLEXUS_HOME) == null) {
            // TODO: this will not work always ...
            // maybe change ConfigReader to try cp resources, too?
            System.setProperty(ConfigReader.ENV_PLEXUS_HOME,
                    "src/test/resources");
        }
    }

    @Test
    public void testGetInstance() {
        final ConfigReader cfg = ConfigReader.getInstance();
        assertNotNull(cfg);
        assertTrue(cfg == ConfigReader.getInstance()); // yes I want ==
    }

    @Test
    public void testGetConfig() {
        // TODO: fails in continuum - config not found ...
        final ConfigReader cfg = ConfigReader.getInstance();
        assertNotNull(cfg);
        assertNull(cfg.getConfig("UNKOWN_VALUE"));

        // read values from src/test/resouces/conf/gforge.cnf
        assertEquals("testVal0", cfg.getConfig("testKey0"));
        assertEquals(null, cfg.getConfig("testKey1"));
    }

    @Test
    public void testGetAdmins() {
        // TODO: fails in continuum - config not found ...
        final ConfigReader cfg = ConfigReader.getInstance();
        assertNotNull(cfg);

        // read values from src/test/resouces/conf/gforge.cnf
        final List<String> admins = cfg.getAdmins();
        assertNotNull(admins);
        assertEquals(2, admins.size());
        assertTrue(admins.contains("admin0"));
        assertTrue(admins.contains("admin1"));

    }

}
