/*
 * evolvis-auth-cis,
 * A implementation of plexus redback for GForge-Authentication
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-auth-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.plexus.security.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Hashtable;

import org.codehaus.plexus.redback.rbac.Permission;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import de.tarent.plexus.security.config.ConfigReaderTest;

/**
 * Note: This tests depends on external systems (as in
 * http://evolvis.org/soap/index.php must be reachable for this test to work)
 * 
 * @author Fabian Bieker
 */
@Ignore("need a mock ws / config")
public class GforgeEntityManagerTest {

    private GforgeEntityManager m;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        m = GforgeEntityManager.getInstance();
        ConfigReaderTest.setEnv();
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getAllPermissions()}
     * .
     */
    @Test
    public void testGetAllPermissions() {
        Hashtable<String, Permission> p = m.getAllPermissions();
        assertNotNull(p);
        // TODO: ...
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getAllOperations()}
     * .
     */
    @Test
    public void testGetAllOperations() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getAlluserAssignments()}
     * .
     */
    @Test
    public void testGetAlluserAssignments() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getAllResources()}
     * .
     */
    @Test
    public void testGetAllResources() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getAllRoles()}
     * .
     */
    @Test
    public void testGetAllRoles() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getResource(java.lang.String)}
     * .
     */
    @Test
    public void testGetResource() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getPermission(java.lang.String)}
     * .
     */
    @Test
    public void testGetPermission() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getRoleByName(java.lang.String)}
     * .
     */
    @Test
    public void testGetRoleByName() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getOperation(java.lang.String)}
     * .
     */
    @Test
    public void testGetOperation() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getUserassignment(java.lang.String)}
     * .
     */
    @Test
    public void testGetUserassignment() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#checkIfEntytyShouldBeUpdatet(java.lang.String)}
     * .
     */
    @Test
    public void testCheckIfEntytyShouldBeUpdatet() {
        fail("Not yet implemented");
    }

    /**
     * Test method for
     * {@link de.tarent.plexus.security.entities.GforgeEntityManager#getInstance()}
     * .
     */
    @Test
    public void testGetInstance() {
        final GforgeEntityManager m = GforgeEntityManager.getInstance();
        assertNotNull(m);
        assertTrue(m == GforgeEntityManager.getInstance()); // yes, ==
    }

}
